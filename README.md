
### How to deploy Discordrb to heroku ?

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)  

1. clone this repo
2. add your token and userid in config/config.rb
3. deploy
4. open console bash in website heroku 
5. or alternative via bash git your pc ```heroku run bash``` ( need [git](https://git-scm.com/) and [heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) )
6. execute in bash ```ruby linkstart.rb```
#### VOILAA.. bot running
![](https://i.imgur.com/pDXB8GQ.jpg)

### nb:
- you need add-ons [Heroku Scheduler](https://elements.heroku.com/addons/scheduler) to continuously run bot ,it's like a cronjob. your account heroku need verification CreditCard to use.

#### thanks
